<?php

use Thrift\ClassLoader\ThriftClassLoader;

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\TBufferedTransport;
use Thrift\Transport\TSocket;

require_once __DIR__ . '/src/lib/ClassLoader/ThriftClassLoader.php';

$loader = new ThriftClassLoader();
$loader->registerNamespace('Thrift', __DIR__ . '/src/lib');
$loader->register();

require_once __DIR__ . '/src/Types.php';
require_once __DIR__ . '/src/THBaseService.php';


$socket = new TSocket('47.92.194.207', '9090');

$socket->setSendTimeout(10000); // Ten seconds (too long for production, but this is just a demo ;)
$socket->setRecvTimeout(20000); // Twenty seconds
$transport = new TBufferedTransport($socket);
$protocol = new TBinaryProtocol($transport);
$client = new THBaseServiceClient($protocol);

$transport->open();
$tableName = "user";

$get = new TGet();
$get->row = "1";

$arr = $client->get($tableName, $get);
$data = array();
$results = $arr->columnValues;
foreach ($results as $result) {
	$qualifier = (string)$result->qualifier;
	$value = $result->value;
	$data[$qualifier] = $value;
}
$transport->close();

var_dump($data);
